import time

class Display:
    Exit = True
    Running = False

    NUMBER = 0

    def show_button(self, num):
        self.NUMBER = num

    def __init__(self):
        self.Exit = False
        self.Running = True
        self.NUMBER = 0

    def run(self):
        self.Exit = False
        self.Running = True

        number = -1

        while self.Exit is False:
            if number != self.NUMBER:
                number = self.NUMBER
                str = "New Number = {:d}".format(self.NUMBER)
                print(str)

            time.sleep(1)

        self.Running = False
