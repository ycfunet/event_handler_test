from events import Events

import time

class Button:

    Exit = True
    Running = False

    EVENTS = None

    def __init__(self, events):
        self.EVENTS = events

    def run(self):
        self.Exit = False
        self.Running = True

        while self.Exit is False:
            self.EVENTS.on_change(6)

            time.sleep(1)

        self.Running = False
