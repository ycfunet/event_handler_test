# Python版本的Event-Handler (connect-slots)，要先安裝套件events。
# 使用步驟：
# 1. 設定Signal function Name
# self.events = Events(('on_change', 'on_trigger'))
# on_change, on_trigger都是Signal function Name
#
# 2. 發出Signal的來源端要把Events指定進去(Slot端不用)
# self.button = Button(self.events)
# self.display = Display()
#
# 3. 實做Slot function
# class Display:
# .....
#     def show_button(self, num):
#         self.NUMBER = num
# .....
#
# 4. 將Slot function和Signal function連結
# self.events.on_change += self.display.show_button
#
# 5. 觸發Signal function
# class Button:
# .....
#     EVENTS = None
# .....
#     def __init__(self, events):
#         self.EVENTS = events
# .....
#     def run(self):
# .....
#         self.EVENTS.on_change(6)
# .....
# 看得出來沒有Thread-Safe，因此如果要用在多執行緒內，要另外加上mutex。

from button_func import Button
from display_func import Display

from events import Events

import threading

class Main:

    events = None

    display = None
    button = None

    def __init__(self):
        self.events = Events(('on_change', 'on_trigger'))
        self.display = Display()
        self.button = Button(self.events)
        self.events.on_change += self.display.show_button

    def run(self):
        display_thread = threading.Thread(target=self.display.run)
        button_thread = threading.Thread(target=self.button.run)
        display_thread.start()
        button_thread.start()

if __name__ == '__main__':
    main = Main()
    main.run()
